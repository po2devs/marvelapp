package po2devs.marvelapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    private ListView lv;
   public static ArrayList<characterInfo> characterList;
    TableLayout tl;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        characterList = new ArrayList<characterInfo>();
        setContentView(R.layout.activity_main);


        ctx = this;
        tl = (TableLayout) findViewById(R.id.table);
        new GetContacts().execute();


    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {

            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();


            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this, "Json Data is downloading", Toast.LENGTH_LONG).show();

        }




        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            String privateKey = "2d0cfa2b841c04cda9387f373d7590c24c21cd87";
            String publicKey = "f230a4d21d7a477ebbd6ae8a1d84eb0a";
            String tomd5 = "1"+privateKey+""+publicKey;
            String hash = md5(tomd5);

            String url = "https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey="+publicKey+"&hash="+hash;
            System.out.println(url);

            String jsonStr = sh.makeServiceCall(url);


            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONObject jo = jsonObj.getJSONObject("data");
                    JSONArray results = jo.getJSONArray("results");

                    for (int i = 0; i < results.length(); i++) {
                        JSONObject c = results.getJSONObject(i);
                        String id = c.getString("id");
                        String name = c.getString("name");
                        String description = c.getString("description");
                        JSONObject c1 = c.getJSONObject("thumbnail");
                        String path = c1.getString("path");
                        String ext = c1.getString("extension");

                        characterInfo cInfo = new characterInfo(path+"."+ext,name,description);
                        characterList.add(cInfo);
                       //sLog.v("ASDSD",i + ") " + path);

                    }


                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                         //   Toast.makeText(getApplicationContext(),
                              //      "Json parsing error: " + e.getMessage(),
                              //      Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);


            for (int i = 0; i < characterList.size(); i++) {

            String name = characterList.get(i).name;
            final int pos = i;
            TableRow tableRow = new TableRow(ctx);
                tableRow.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        Intent myIntent = new Intent(MainActivity.this, showCharacter.class);
                        myIntent.putExtra("i",pos);
                        MainActivity.this.startActivity(myIntent);

                    }
                });



                TableLayout.LayoutParams tableRowParams=
                        new TableLayout.LayoutParams
                                (TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);

                int leftMargin=10;
                int topMargin=2;
                int rightMargin=64;
                int bottomMargin=32;

                tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);

                tableRow.setLayoutParams(tableRowParams);

               TextView textView = new TextView(ctx);

                textView.setText(name);


                tableRow.addView(textView);

                tableRow.setClickable(true);


                tl.addView(tableRow);



            }





         /*  ListAdapter adapter = new SimpleAdapter(MainActivity.this, contactList,
                    R.layout.character_item, new String[]{"email", "mobile"},
                    new int[]{R.id.email, R.id.mobile});



           // lv.setAdapter(adapter);
           */
        }






    }






    }
