package po2devs.marvelapp;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class showCharacter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_character);

        ImageView iv =(ImageView)findViewById(R.id.imageView2);
        int i = getIntent().getExtras().getInt("i");
        String imgURL = MainActivity.characterList.get(i).imgURL;
        String name = MainActivity.characterList.get(i).name;
        String description = MainActivity.characterList.get(i).description;

        Picasso.with(this).load(imgURL).into(iv);


        TextView tv = (TextView)findViewById(R.id.textView);
        tv.setText(name);

        TextView tv1 = (TextView)findViewById(R.id.textView2);
        tv1.setText(description);
        tv1.setFocusable(false);
        tv1.setClickable(false);

    }
}
